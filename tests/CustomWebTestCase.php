<?php

namespace App\Tests;

use App\Repository\ApiTokenRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class CustomWebTestCase extends WebTestCase
{

    protected static $apiTokens = [];

    /**
     * @var KernelBrowser $client
     */
    protected $client;

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ApiTokenRepository
     */
    private $tokenRepository;

    protected function setUp()
    {
        parent::setUp();
        $this->client = static::createClient();
    }

    /**
     * Returns response content
     * @return array
     */
    protected function getJsonResponse()
    {
        return json_decode($this->client->getResponse()->getContent(), true);
    }

    /**
     * @param int|null $userId
     * @return string
     * @throws \Exception
     */
    protected function getApiToken(int $userId)
    {
        if (isset(self::$apiTokens[$userId])) {
            return self::$apiTokens[$userId];
        }

        if (empty($this->userRepository)) {
            $this->userRepository = static::$container->get(UserRepository::class);
        }

        $user = $this->userRepository->find($userId);

        if (null === $user) {
            throw new \Exception(sprintf('User %d not found', $userId));
        }

        if (empty($this->tokenRepository)) {
            $this->tokenRepository = static::$container->get(ApiTokenRepository::class);
        }

        $newToken = $this->tokenRepository->create($user)->getToken();
        self::$apiTokens[$userId] = $newToken;

        return $newToken;
    }

    /**
     * Check if validation fail occurred
     * @param array $expectedInvalidFields
     * @param array $expectedValidFields
     */
    protected function assertValidationFailed(array $expectedInvalidFields = [], array $expectedValidFields = [])
    {
        $body = $this->getJsonResponse();

        $this->assertEquals("Request parameters didn't validate.", $body['title']);

        foreach ($expectedInvalidFields as $expectedInvalidField) {
            $this->assertArrayHasKey($expectedInvalidField, $body['invalid-params']);
        }

        foreach ($expectedValidFields as $expectedValidField) {
            $this->assertArrayNotHasKey($expectedValidField, $body['invalid-params']);
        }
    }

    protected function apiCall(string $method, string $path, string $token = null, array $body = [])
    {
        $headers = [];
        if (isset($token)) {
            $headers['HTTP_AUTHORIZATION'] = 'Bearer '.$token;
        }

        return $this->client->request(
            $method,
            $path,
            [],
            [],
            $headers,
            json_encode($body)
        );
    }
}
