<?php

namespace App\Tests\Controller;

use App\Tests\CustomWebTestCase;

class SecurityControllerTest extends CustomWebTestCase
{

    /**
     * TODO: proper tests...
     */

    /**
     * @covers \App\Controller\SecurityController
     */
    public function testLogin()
    {
        $this->apiCall(
            'POST',
            '/token',
            null,
            [
                'email' => 'test@test.test',
                'password' => 'invalid',
            ]
        );

        $this->assertResponseStatusCodeSame(401);

        $this->apiCall(
            'POST',
            '/token',
            null,
            [
                'email' => 'test@test.test',
                'password' => 'password',
            ]
        );

        $response = $this->getJsonResponse();
        $this->assertResponseStatusCodeSame(201);

        self::$apiTokens[0] = $response['data']['token'];
    }

    /**
     * @covers  \App\Controller\SecurityController
     * @depends testLogin
     */
    public function testLogout()
    {
        $this->apiCall(
            'DELETE',
            '/token',
            'invalid'
        );

        $this->assertResponseStatusCodeSame(401);

        $this->apiCall(
            'DELETE',
            '/token',
            self::$apiTokens[0]
        );

        $this->assertResponseStatusCodeSame(200);
    }

}