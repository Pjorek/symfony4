<?php

namespace App\Tests\Controller;

use App\Tests\CustomWebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserControllerTest extends CustomWebTestCase
{

    static protected $userId;

    /**
     * TODO: proper tests...
     */

    /**
     * @covers \App\Controller\UserController
     */
    public function testCreate(): void
    {
        $this->apiCall(
            'POST',
            '/user',
            null,
            [
                'email' => 'notanemail',
                'password' => 'password',
            ]
        );

        $this->assertResponseStatusCodeSame(400);

        $email = 'test_'.time().'_'.rand(0, 9999).'@test.test';

        $this->apiCall(
            'POST',
            '/user',
            null,
            [
                'email' => $email,
                'password' => 'password',
            ]
        );

        $response = $this->getJsonResponse();
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);

        self::$userId = $response['data']['id'];
    }

    /**
     * @covers  \App\Controller\UserController
     * @depends testCreate
     * @throws \Exception
     */
    public function testShow()
    {
        $this->apiCall(
            'GET',
            '/user/'.self::$userId,
            "badtoken"
        );

        $this->assertResponseStatusCodeSame(401);

        $this->apiCall(
            'GET',
            '/user/1',
            $this->getApiToken(self::$userId)
        );

        $this->assertResponseStatusCodeSame(403);

        $this->apiCall(
            'GET',
            '/user/'.(self::$userId + 1),
            $this->getApiToken(self::$userId)
        );

        $this->assertResponseStatusCodeSame(404);

        $this->apiCall(
            'GET',
            '/user/'.self::$userId,
            $this->getApiToken(self::$userId)
        );

        $this->assertResponseStatusCodeSame(200);
    }

    /**
     * @covers  \App\Controller\UserController
     * @depends testCreate
     * @throws \Exception
     */
    public function testUpdate()
    {
        $email = 'test_x'.time().'_'.rand(0, 9999).'@test.test';

        $this->apiCall(
            'PUT',
            '/user/'.self::$userId,
            "badtoken"
        );

        $this->assertResponseStatusCodeSame(401);

        $this->apiCall(
            'PUT',
            '/user/'.self::$userId,
            $this->getApiToken(self::$userId),
            [
                'email' => '',
            ]
        );

        $this->assertResponseStatusCodeSame(400);

        $this->apiCall(
            'PUT',
            '/user/'.self::$userId,
            $this->getApiToken(self::$userId),
            [
                'email' => $email,
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->apiCall(
            'PUT',
            '/user/'.(self::$userId + 1),
            $this->getApiToken(self::$userId),
            [
                'email' => $email,
            ]
        );

        $this->assertResponseStatusCodeSame(404);

        $this->apiCall(
            'PUT',
            '/user/1',
            $this->getApiToken(self::$userId),
            [
                'email' => $email,
            ]
        );

        $this->assertResponseStatusCodeSame(403);
    }

    /**
     * @covers  \App\Controller\UserController
     * @depends testCreate
     * @throws \Exception
     */
    public function testDelete(): void
    {

        $this->apiCall(
            'DELETE',
            '/user/'.self::$userId,
            "badtoken"
        );

        $this->assertResponseStatusCodeSame(401);

        $this->apiCall(
            'DELETE',
            '/user/'.(self::$userId + 1),
            $this->getApiToken(self::$userId)
        );

        $this->assertResponseStatusCodeSame(404);

        $this->apiCall(
            'DELETE',
            '/user/1',
            $this->getApiToken(self::$userId)
        );

        $this->assertResponseStatusCodeSame(403);

        $this->apiCall(
            'DELETE',
            '/user/'.self::$userId,
            $this->getApiToken(self::$userId)
        );

        $this->assertResponseStatusCodeSame(200);

        $this->apiCall(
            'DELETE',
            '/user/'.self::$userId,
            $this->getApiToken(self::$userId)
        );

        $this->assertResponseStatusCodeSame(401);
    }

}