<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class JsonController extends AbstractController
{

    /**
     * @var LoggerInterface $logger
     */
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * todo: check for potential information leaks
     * Formatter for Validator error response
     *
     * @param ConstraintViolationListInterface $errors
     * @return JsonResponse
     */
    protected function validationFailure(ConstraintViolationListInterface $errors)
    {
        $formattedErrors = [];
        foreach ($errors as $error) {
            /**
             * @var ConstraintViolation $error
             */
            $formattedErrors[$error->getPropertyPath()] = [
                'reason' => $error->getMessage(),
                'code' => $error->getCode(),
                'value' => $error->getInvalidValue(),
            ];
        }

        $this->logger->error('Input Validation Failure!', $formattedErrors);

        return $this->json($formattedErrors, Response::HTTP_BAD_REQUEST);
    }

    protected function resourceNotFound(string $resource, int $id)
    {
        return $this->json(
            [
                'message' => 'Resource not found',
                'resource' => $resource,
                'id' => $id,
            ],
            Response::HTTP_NOT_FOUND
        );
    }

    protected function accessDenied()
    {
        return $this->json(['message' => 'Access denied'], Response::HTTP_FORBIDDEN);
    }

    protected function success($data = [], int $code = 200)
    {
        return $this->json(
            [
                'message' => 'Success',
                'data' => $data,
            ],
            $code
        );
    }
}