<?php

namespace App\Controller;

use App\Repository\ApiTokenRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends JsonController
{

    public function __construct(LoggerInterface $logger)
    {
        parent::__construct($logger);
    }

    /**
     * @Route("/token", name="token_create", methods={"POST"})
     */
    public function login()
    {
        //handled in LoginAuthenticator
    }

    /**
     * @Route("/token", name="token_remove", methods={"DELETE"})
     * @param Request $request
     * @param ApiTokenRepository $tokenRepository
     * @return JsonResponse
     */
    public function logout(Request $request, ApiTokenRepository $tokenRepository)
    {
        //todo: there has to be a way to do this without dirty, dirty hacks... Find it.
        $rawToken = substr($request->headers->all()['authorization'][0], 7);
        $token = $tokenRepository->findOneBy(['token' => $rawToken]);

        $em = $this->getDoctrine()->getManager();
        $em->persist($token);
        $em->flush();

        return $this->success();
    }
}
