<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends JsonController
{

    /**
     * @var ValidatorInterface $validator
     */
    protected $validator;
    /**
     * @var UserRepository $userRepository
     */
    private $userRepository;

    public function __construct(
        LoggerInterface $logger,
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct($logger);
        $this->validator = $validator;
        $this->userRepository = $entityManager->getRepository(User::class);
    }

    /**
     * Fetch user data
     *
     * @Route("/user/{id}", name="user_show", methods={"GET"}, requirements={"id"="\d+"})
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id = 0)
    {
        if (empty($id)) {
            return new JsonResponse($this->getUser());
        }

        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->resourceNotFound('user', $id);
        }

        if (!$this->isGranted(['SELF'], $user) && !$this->isGranted(['ROLE_ADMIN'])) {
            return $this->accessDenied();
        }

        return $this->success($user);
    }

    /**
     * Create new user
     *
     * @Route("/user", name="user_create", methods={"POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return JsonResponse
     */
    public function create(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder
    ): Response {
        //todo: two step sign up with email verification
        $data = json_decode($request->getContent(), true);

        $user = new User();
        $encodedPassword = $passwordEncoder->encodePassword($user, $data['password']);
        $user->setPassword($encodedPassword);
        $user->setEmail($data['email']);
        $user->setRoles([]);

        $errors = $this->validator->validate($user);

        if (0 < count($errors)) {
            return $this->validationFailure($errors);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->success($user, Response::HTTP_CREATED);
    }

    /**
     * Update user data.
     *
     * @Route("/user/{id}", name="user_update", methods={"PUT"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(
        Request $request,
        int $id
    ): Response {
        //todo: email verification on change
        $data = json_decode($request->getContent(), true);
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->resourceNotFound('user', $id);
        }

        if (!$this->isGranted(['SELF'], $user) && !$this->isGranted(['ROLE_ADMIN'])) {
            return $this->accessDenied();
        }

        $user->setEmail(isset($data['email']) ? $data['email'] : '');

        $errors = $this->validator->validate($user);

        if (0 < count($errors)) {
            return $this->validationFailure($errors);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->success($user);
    }

    /**
     * @Route("/user/{id}", name="user_delete", methods={"DELETE"}, requirements={"id"="\d+"})
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): Response
    {
        $user = $this->userRepository->find($id);
        if (empty($user)) {
            return $this->resourceNotFound('user', $id);
        }

        if (!$this->isGranted(['SELF'], $user) && !$this->isGranted(['ROLE_ADMIN'])) {
            return $this->accessDenied();
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $response = ['id' => $id];

        return $this->success($response);
    }
}
