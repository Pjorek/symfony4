<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

abstract class JsonGuardAuthenticator extends AbstractGuardAuthenticator
{

    //todo: input validation?

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return $this->returnFailure($authException->getMessage());
    }

    protected function returnFailure(string $message, int $code = 400)
    {
        return new JsonResponse(
            [
                'message' => $message,
            ], $code
        );
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
