<?php

namespace App\Security;

use App\Entity\User;
use App\Repository\ApiTokenRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class LoginAuthenticator extends JsonGuardAuthenticator
{


    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var ApiTokenRepository
     */
    private $apiTokenRepository;

    public function __construct(
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        ApiTokenRepository $apiTokenRepository
    ) {
        $this->passwordEncoder = $passwordEncoder;
        $this->userRepository = $userRepository;
        $this->apiTokenRepository = $apiTokenRepository;
    }

    public function supports(Request $request)
    {
        return true;
    }

    /**
     * @param Request $request
     * @return array|mixed
     */
    public function getCredentials(Request $request)
    {
        $params = json_decode($request->getContent(), true);

        if (empty($params['email']) || empty($params['password'])) {
            throw new CustomUserMessageAuthenticationException("Credentials missing");
        }

        return [
            'email' => $params['email'],
            'password' => $params['password'],
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $user = $this->userRepository->findByEmail($credentials['email']);

        if (null === $user) {
            //todo: fake password validation to protect against timing attacks
            throw new CustomUserMessageAuthenticationException(
                'Authorisation failed',
                [],
                Response::HTTP_UNAUTHORIZED
            );
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        if (true !== $this->passwordEncoder->isPasswordValid($user, $credentials['password'])) {
            throw new CustomUserMessageAuthenticationException(
                'Authorisation failed',
                [],
                Response::HTTP_UNAUTHORIZED
            );
        }

        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $code = $exception->getCode();

        return new JsonResponse(
            ['message' => $exception->getMessage()],
            !empty($code) ? $code : Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param $providerKey
     * @return JsonResponse|Response|null
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        /**
         * @var User $user
         */
        $user = $token->getUser();
        $apiToken = $this->apiTokenRepository->create($user);

        return new JsonResponse(
            [
                'message' => 'Success',
                'data' => [
                    'token' => $apiToken->getToken(),
                    'user' => $user,
                ],
            ],
            Response::HTTP_CREATED
        );
    }
}
