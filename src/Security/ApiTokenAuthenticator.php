<?php

namespace App\Security;

use App\Repository\ApiTokenRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ApiTokenAuthenticator extends JsonGuardAuthenticator
{
    /**
     * @var ApiTokenRepository
     */
    private $apiTokenRepository;

    public function __construct(ApiTokenRepository $apiTokenRepository)
    {
        $this->apiTokenRepository = $apiTokenRepository;
    }

    public function supports(Request $request)
    {
        if (false === $request->headers->has('Authorization')) {
            throw new CustomUserMessageAuthenticationException(
                "No auth header!",
                [],
                Response::HTTP_UNAUTHORIZED
            );
        }

        $authorisationParts = explode(' ', $request->headers->get('Authorization'));
        if (2 !== count($authorisationParts)
            || 'Bearer' !== reset($authorisationParts)) {
            throw new CustomUserMessageAuthenticationException("Malformed auth header!");
        }

        return true;
    }

    public function getCredentials(Request $request)
    {
        $authorisation = $request->headers->get('Authorization');

        return substr($authorisation, 7);
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = $this->apiTokenRepository->findOneBy(
            [
                'token' => $credentials,
            ]
        );

        if (!$token) {
            throw new CustomUserMessageAuthenticationException(
                'Invalid API Token',
                [],
                Response::HTTP_UNAUTHORIZED
            );
        }

        return $token->getUser();
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new JsonResponse(
            [
                'message' => $exception->getMessageKey(),
            ], $exception->getCode()
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        //continue
    }


}
